from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
import uvicorn

from app import schemas
from detect import detect
from ops.utils.general import check_requirements, strip_optimizer
from ops.utils.torch_utils import torch_distributed_zero_first
from typing import List

app = FastAPI(title='Tivra Camera App')

@app.get('/')
def index():
    return {'message': 'This is Tivra Camera API!'}

@app.post('/detect/')
def detect_stream(data:schemas.DetectStart):
    data = data.dict()
    source = data['source']
    weights= data['weights']

    if weights == 'string' or weights == "":
        weights = "ops/modelA.pt"
    detect(source=source, weights=weights) 

    return {
        'dectection completed': "detection saved in runs directory"
    }


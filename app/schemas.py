from typing import List, Optional


from pydantic import BaseModel



class DetectBase(BaseModel):

    source: str
    weights:str

class DetectStart(DetectBase):
    pass

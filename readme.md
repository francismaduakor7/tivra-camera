# Tivra-Camera API

The API has the main endpoint with is: 
  
  * http://54.169.142.91/detect/

This endpoint accepts only a POST method and is used for performing detection on a data.

* **Source** - Source is the data source can be an image, video or can end with rtsp://', 'rtmp://', 'http://', 'https://'

* **Weights** -  Weights is the trained weight to be used for the inference on the data.  We weight is left as "string" or "". The API would use the default weight. To use a new or custom weight, the weight file has to be uploaded to the EC2 Instance and the path is use as the weights when calling the detect endpoint.  


## Example:


*CURL*

curl -X 'POST' \
  'http://54.169.142.91/detect/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "source": "frame_07050.png",
  "weights": "string"
}'


*PYTHON*

import requests

headers = {
    'accept': 'application/json', \
    'Content-Type': 'application/json', \
} \
data = '{ "source": "frame_07050.png", "weights": "string" }' \
response = requests.post('http://54.169.142.91/detect/', headers=headers, data=data) \



## Documentations

You can get a easy-to-use and easy-to-read documentation of the API use one of the following:

* http://54.169.142.91/docs

* http://54.169.142.91/redoc


